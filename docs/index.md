---
layout: home

hero:
  name: "hanzn-zzx"
  text: "hanzn-zzx的文档站"
  actions:
    - theme: brand
      text: 文档主页
      link: /home
    - theme: alt
      text: Linux入门教程
      link: /wiki/introduction
---
