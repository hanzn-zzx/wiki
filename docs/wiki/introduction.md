# Linux入门教程
## 关于文档
这是一份针对Linux入门新手而写的文档，因而会以更加通俗的方式来讲述，如有错误可以在issues中指正，或者提交pr

若无特殊说明，本教程所使用的Linux发行版为deepin，部分涉及Ubuntu/Debian/AOSC OS

## 版权声明
若无特殊声明，全部文档依照CC-BY-NC协议发布

## 一个小小的介绍
### 